package main;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class MadisonIslandPage extends ScreenShooter {
    public SelenideElement homepageLogo = $(".logo .large");

    public SelenideElement searchInput = $("#search");

    public SelenideElement accountLibrary = $(".skip-content.skip-active");
    public SelenideElement createAccountPage = $("div.account-create");
    public SelenideElement firstNameBox = $("input#firstname");
    public SelenideElement lastNameBox = $("input#lastname");
    public SelenideElement emailBox = $("input#email_address");
    public SelenideElement passwordBox = $("input#password");
    public SelenideElement passwordConfirmationBox = $("input#confirmation");
    public SelenideElement registerAccountButton = $(".buttons-set .button");

    public SelenideElement loginButton = $("#header-account .last");
    public SelenideElement emailInput = $("#email");
    public SelenideElement passwordInput = $("#pass");
    public SelenideElement confirmLoginButton = $("#send2");

    public SelenideElement loginErrorMessage = $("li.error-msg");
    public SelenideElement emailRequired = $("#advice-required-entry-email");
    public SelenideElement passwordRequired = $("#advice-required-entry-pass");
    public SelenideElement forgotYourPasswordButton = $(".f-left");
    public SelenideElement emailForPasswordReset = $("#email_address");
    public SelenideElement submitButton = $("div.buttons-set .button");
    public SelenideElement resetPasswordMessage = $("li.success-msg");

    public SelenideElement logoutButton = $("#header-account .last");

    public SelenideElement searchButton = $(".search-button");
    public SelenideElement productSticlaInox = $("h2.product-name");


    public SelenideElement menCategory = $(".nav-2");
    public SelenideElement viewAllMenSubcategory = $(".level1 [href='http://testfasttrackit.info/magento-test/men.html']");

    public SelenideElement addToCartButton = $(".add-to-cart-buttons");
    public SelenideElement cartItemsNumber = $(".header-minicart .count");
    public SelenideElement cartButton = $(".header-minicart");
    public SelenideElement emptyCartButton = $("#empty_cart_button");
    public SelenideElement addToCartQuantity = $(".add-to-cart .qty");

    public ElementsCollection allMenProducts = $$(".category-products .product-name");
    public ElementsCollection wishlistButtons = $$(".actions .link-wishlist");
    public ElementsCollection productsInWishlist = $$(".fieldset .product-name");

    public SelenideElement selectBlackColor = $("[alt='Black']");
    public SelenideElement selectSizeS = $("[title='S']");
    public SelenideElement itemOptions = $(".cart-table.data-table .item-options");
    public SelenideElement subtotal = $(".product-cart-total .price");


    public ElementsCollection removeFromCartButtons = $$(".a-center.product-cart-remove.last .btn-remove.btn-remove2");


    public void isFirstNameBoxVisible() {
        firstNameBox.shouldBe(visible);
    }

    public void fillInFirstNameBox(String input) {
        firstNameBox.sendKeys(input);
    }

    public void isLastNameVisible() {
        lastNameBox.shouldBe(visible);
    }

    public void fillInLastNameBox(String input) {
        lastNameBox.sendKeys(input);
    }

    public void isEmailBoxVisible() {
        emailBox.shouldBe(visible);
    }

    public void fillInEmailBox(String input) {
        emailBox.sendKeys(input);
    }

    public void isPasswordVisible() {
        passwordBox.shouldBe(visible);

    }

    public void fillInPasswordBox(String input) {
        passwordBox.sendKeys(input);
    }

    public void isPasswordConfirmationBoxVisible() {
        passwordConfirmationBox.shouldBe(visible);
    }

    public void fillInPasswordConfirmationBox(String input) {
        passwordConfirmationBox.sendKeys(input);
    }

    public void isRegisterAccountButtonVisible() {
        registerAccountButton.shouldBe(visible);
    }

    public void clickOnRegisterAccountButton() {
        registerAccountButton.click();
    }

    public void isHomepageLogoVisible() {
        homepageLogo.shouldBe(visible);
    }

    public void clickOnHomepageLogo() {
        homepageLogo.click();
    }

    public void isLogoutButtonVisible() {
        logoutButton.shouldBe(visible);
    }

    public void clickOnLogOutButton() {
        logoutButton.click();
    }

    public void isLoginButtonVisible() {
        loginButton.shouldBe(visible);
    }

    public void clickOnLoginButton() {
        loginButton.click();
    }

    public void isEmailInputVisible() {
        emailInput.shouldBe(visible);
    }

    public void fillInEmailInput(String input) {
        emailInput.sendKeys(input);
    }

    public void isPasswordInputVisible() {
        passwordInput.shouldBe(visible);
    }

    public void fillInPasswordInput(String input) {
        passwordInput.sendKeys(input);
    }

    public void isConfirmLoginButtonVisible() {
        confirmLoginButton.shouldBe(visible);
    }

    public void clickOnConfirmLoginButton() {
        confirmLoginButton.click();
    }

    public void isLoginErrorMessageVisible() {
        loginErrorMessage.shouldBe(visible);
    }

    public void LoginTextShouldHaveExactErrorText(String input) {
        loginErrorMessage.shouldHave(exactText(input));
    }

    public void hoverOverMenCategory() {
        menCategory.hover();
    }

    public void isViewAllMenSubcategoryVisible() {
        viewAllMenSubcategory.shouldBe(visible);
    }

    public void clickOnViewAllMenSubcategory() {
        viewAllMenSubcategory.click();
    }


    public void isAddToCartButtonVisible() {
        addToCartButton.shouldBe(visible);
    }

    public void clickOnAddToCartButton() {
        addToCartButton.click();
    }

    public void isCartItemsNumberVisible() {
        cartItemsNumber.shouldBe(visible);
    }

    public void verifyCartItemsNumber(String input) {
        cartItemsNumber.shouldHave(exactText(input));
    }

    public void isCartButtonVisible() {
        cartButton.shouldBe(visible);
    }

    public void clickOnCartButton() {
        cartButton.click();
    }

    public void isEmptyCartButtonVisible() {
        emptyCartButton.shouldBe(visible);
    }

    public void clickOnEmptyCartButton() {
        emptyCartButton.click();
    }

    public void isAddToCartQuantityVisible() {
        addToCartQuantity.shouldBe(visible);
    }

    public void doubleClickOnAddToCartQuantity() {
        addToCartQuantity.doubleClick();
    }

    public void fillInAddToCartQuantity(String input) {
        addToCartQuantity.sendKeys(input);
    }


    public void verifyWelcomeMessageByText(String welcomeText) {
        SelenideElement welcomeMessage = $("p.welcome-msg");
        welcomeMessage.shouldHave(exactText(welcomeText));
    }

    public void clickOnAccountButton() {
        SelenideElement accountButton = $("a.skip-link.skip-account");
        accountButton.click();
    }

    public void clickOnRegisterButton() {
        SelenideElement registerButton = $("a[title='Register']");
        registerButton.shouldBe(visible).click();
    }


}
