package org.fasttrackit.tauwebshopmagento;

import com.codeborne.selenide.*;
import io.qameta.allure.Feature;
import main.MadisonIslandPage;
import main.ScreenShooter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class MadisonIslandTests extends ScreenShooter {
    MadisonIslandPage mainPage = new MadisonIslandPage();


    @BeforeClass
    public static void setUpAll() {
        Configuration.browserSize = "1280x800";

    }

    @BeforeMethod
    public void setUp() {
        open("http://testfasttrackit.info/magento-test/");
    }

    @Feature("Homepage")
    @Test
    public void I_expect_a_welcome_message() {
        String welcomeText = "Welcome";
        mainPage.verifyWelcomeMessageByText(welcomeText);
        takeScreenShot("Welcome message is visible.");

    }

    @Feature("Homepage")
    @Test
    public void homepage_should_be_translated_to_french() {
        SelenideElement selectLanguageButton = $("#select-language");
        selectLanguageButton.shouldBe(visible).click();
        selectLanguageButton.sendKeys("f");
        selectLanguageButton.click();
        mainPage.verifyWelcomeMessageByText("Bonjour");
        takeScreenShot("Homepage is not translated to french.");
    }

    @Feature("Homepage")
    @Test
    public void homepage_logo_is_visible() {
        mainPage.isHomepageLogoVisible();
        takeScreenShot("Homepage logo is visible.");
        mainPage.clickOnHomepageLogo();
    }

    @Feature("Register")
    @Test
    public void I_can_open_register_page() {
        mainPage.clickOnAccountButton();
        mainPage.accountLibrary.shouldBe(visible);
        mainPage.clickOnRegisterButton();
        mainPage.createAccountPage.shouldBe(visible);
        takeScreenShot("I can open register page.");
        mainPage.clickOnHomepageLogo();

    }

    @Feature("Register")
    @Test
    public void I_can_create_an_account() {
        mainPage.clickOnAccountButton();
        mainPage.clickOnRegisterButton();
        mainPage.isFirstNameBoxVisible();
        mainPage.fillInFirstNameBox("Silvia");
        mainPage.isLastNameVisible();
        mainPage.fillInLastNameBox("Sabou");
        mainPage.isEmailBoxVisible();
        mainPage.fillInEmailBox("romania@gmail.com");
        mainPage.isPasswordVisible();
        mainPage.fillInPasswordBox("clujnapoca");
        mainPage.isPasswordConfirmationBoxVisible();
        mainPage.fillInPasswordConfirmationBox("clujnapoca");
        mainPage.isRegisterAccountButtonVisible();
        mainPage.clickOnRegisterAccountButton();
        mainPage.isHomepageLogoVisible();
        mainPage.clickOnHomepageLogo();


    }

    @Feature("Login")
    @Test
    public void I_can_login() {
        mainPage.clickOnAccountButton();
        mainPage.isLoginButtonVisible();
        mainPage.clickOnLoginButton();
        mainPage.isEmailInputVisible();
        mainPage.fillInEmailInput("romania@gmail.com");
        mainPage.isPasswordInputVisible();
        mainPage.fillInPasswordInput("clujnapoca");
        mainPage.isConfirmLoginButtonVisible();
        mainPage.clickOnConfirmLoginButton();
        takeScreenShot("I can login.");
        mainPage.clickOnHomepageLogo();

    }

    @Feature("Login")
    @Test(dependsOnMethods = {"I_can_login"})
    public void when_logged_in_welcome_message_contains_users_full_name() {
        String welcomeText = "Welcome, Silvia Sabou!";
        mainPage.verifyWelcomeMessageByText(welcomeText);
        takeScreenShot("Welcome message is visible.");
    }

    @Feature("Logout")
    @Test
    public void I_can_logout() {
        mainPage.clickOnAccountButton();
        mainPage.isLogoutButtonVisible();
        mainPage.clickOnLogOutButton();

    }

    @Feature("Login")
    @Test(dependsOnMethods = {"I_can_logout"})
    public void I_cant_login_with_wrong_credentials() {
        mainPage.clickOnAccountButton();
        mainPage.clickOnLoginButton();
        mainPage.fillInEmailInput("rrmania@gmail.com");
        mainPage.fillInPasswordInput("clujnapocc");
        mainPage.clickOnConfirmLoginButton();
        mainPage.isLoginErrorMessageVisible();
        mainPage.LoginTextShouldHaveExactErrorText("Invalid login or password.");
        takeScreenShot("I can not login with wrong user credentials");


    }

    @Feature("Login")
    @Test(dependsOnMethods = {"I_can_logout"})
    public void I_cant_login_without_credentials() {
        mainPage.clickOnAccountButton();
        mainPage.clickOnLoginButton();
        mainPage.fillInEmailInput("");
        mainPage.fillInPasswordInput("");
        mainPage.clickOnConfirmLoginButton();
        mainPage.emailRequired.shouldBe(visible).shouldHave(exactText("This is a required field."));
        mainPage.passwordRequired.shouldBe(visible).shouldHave(exactText("This is a required field."));
        takeScreenShot("I can not login without user credentials.");
    }

    @Feature("Login")
    @Test(dependsOnMethods = {"I_can_logout"})
    public void I_can_recover_forgotten_password() {
        mainPage.clickOnAccountButton();
        mainPage.clickOnLoginButton();
        mainPage.forgotYourPasswordButton.shouldBe(visible).click();
        mainPage.emailForPasswordReset.shouldBe(visible).sendKeys("romania@gmail.com");
        mainPage.submitButton.shouldBe(visible).click();
        mainPage.resetPasswordMessage.shouldBe(visible);
        mainPage.resetPasswordMessage.shouldHave(exactText("If there is an account associated with romania@gmail.com you will receive an email with a link to reset your password."));
        takeScreenShot("Recover password message");
    }

    @Feature("Search")
    @Test
    public void I_can_use_the_search_function() {
        mainPage.searchInput.shouldBe(visible).click();
        mainPage.searchInput.sendKeys("sticla");
        mainPage.searchButton.shouldBe(visible).click();
        mainPage.productSticlaInox.shouldBe(visible).shouldHave(exactText("STICLA INOX 600 ML FRESCO TWIST&GO"));
        takeScreenShot("Items with the word 'sticla' were found.");


    }

    @Feature("Products")
    @Test
    public void I_can_open_products_details() {
        mainPage.hoverOverMenCategory();
        mainPage.clickOnViewAllMenSubcategory();
        mainPage.allMenProducts.get(0).click();
        takeScreenShot("I can open product's details");
        mainPage.clickOnHomepageLogo();
    }

    @Feature("ShoppingCart")
    @Test
    public void I_can_add_1_item_to_cart() {
        mainPage.hoverOverMenCategory();
        mainPage.isViewAllMenSubcategoryVisible();
        mainPage.clickOnViewAllMenSubcategory();
        mainPage.allMenProducts.get(0).click();
        mainPage.isAddToCartButtonVisible();
        mainPage.clickOnAddToCartButton();
        mainPage.isCartItemsNumberVisible();
        mainPage.verifyCartItemsNumber("1");
        takeScreenShot("I can add 1 item to cart");
        mainPage.isEmptyCartButtonVisible();
        mainPage.clickOnEmptyCartButton();
        mainPage.clickOnHomepageLogo();
    }

    @Feature("ShoppingCart")
    @Test
    public void I_can_add_23_from_the_same_item_to_cart() {
        mainPage.hoverOverMenCategory();
        mainPage.clickOnViewAllMenSubcategory();
        mainPage.allMenProducts.get(0).click();
        mainPage.isAddToCartQuantityVisible();
        mainPage.doubleClickOnAddToCartQuantity();
        mainPage.fillInAddToCartQuantity("23");
        mainPage.clickOnAddToCartButton();
        mainPage.verifyCartItemsNumber("23");
        takeScreenShot("I can add 23 to cart");
        mainPage.clickOnEmptyCartButton();
        mainPage.clickOnHomepageLogo();

    }

    @Feature("ShoppingCart")
    @Test
    public void I_can_add_2_different_products_to_cart() {
        mainPage.hoverOverMenCategory();
        mainPage.clickOnViewAllMenSubcategory();
        mainPage.allMenProducts.get(0).click();
        mainPage.isAddToCartQuantityVisible();
        mainPage.doubleClickOnAddToCartQuantity();
        mainPage.fillInAddToCartQuantity("1");
        mainPage.clickOnAddToCartButton();
        back();
        back();
        mainPage.allMenProducts.get(1).click();
        mainPage.doubleClickOnAddToCartQuantity();
        mainPage.fillInAddToCartQuantity("1");
        mainPage.clickOnAddToCartButton();
        mainPage.verifyCartItemsNumber("2");
        takeScreenShot("I can add 2 different products.");
        mainPage.clickOnEmptyCartButton();
        mainPage.clickOnHomepageLogo();

    }

    @Feature("ShoppingCart")
    @Test
    public void I_can_add_an_item_to_cart_by_selecting_its_color_and_size() {
        mainPage.hoverOverMenCategory();
        mainPage.clickOnViewAllMenSubcategory();
        mainPage.allMenProducts.get(2).click();
        mainPage.selectBlackColor.shouldBe(visible).click();
        mainPage.selectSizeS.shouldBe(visible).click();
        mainPage.clickOnAddToCartButton();
        mainPage.itemOptions.shouldBe(visible);
        takeScreenShot("Item added to cart has black color and size S");
        mainPage.clickOnEmptyCartButton();
        mainPage.clickOnHomepageLogo();
    }

    @Feature("ShoppingCart")
    @Test
    public void I_can_remove_1_item_out_of_2_from_cart() {
        mainPage.hoverOverMenCategory();
        mainPage.clickOnViewAllMenSubcategory();
        mainPage.allMenProducts.get(0).click();
        mainPage.isAddToCartQuantityVisible();
        mainPage.doubleClickOnAddToCartQuantity();
        mainPage.fillInAddToCartQuantity("1");
        mainPage.clickOnAddToCartButton();
        back();
        back();
        mainPage.allMenProducts.get(1).click();
        mainPage.doubleClickOnAddToCartQuantity();
        mainPage.fillInAddToCartQuantity("1");
        mainPage.clickOnAddToCartButton();
        mainPage.removeFromCartButtons.get(0).click();
        mainPage.verifyCartItemsNumber("1");
        takeScreenShot("I can remove 1 item.");
        mainPage.clickOnEmptyCartButton();
        mainPage.clickOnHomepageLogo();
    }

    @Feature("Wishlist")
    @Test(dependsOnMethods = {"I_can_login"})
    public void I_can_add_1_item_to_wishlist() {
        mainPage.hoverOverMenCategory();
        mainPage.clickOnViewAllMenSubcategory();
        mainPage.allMenProducts.get(0).shouldBe(visible);
        mainPage.wishlistButtons.get(0).click();
        mainPage.productsInWishlist.get(0).shouldBe(visible);
        takeScreenShot("I can add 1 item to wishlist.");
        mainPage.clickOnHomepageLogo();
    }

    @Feature("ShoppingCart")
    @Test
    public void subtotal_is_correct() {
        mainPage.hoverOverMenCategory();
        mainPage.clickOnViewAllMenSubcategory();
        mainPage.allMenProducts.get(0).click();
        mainPage.clickOnAddToCartButton();
        mainPage.verifyCartItemsNumber("1");
        mainPage.isCartButtonVisible();
        mainPage.clickOnCartButton();
        mainPage.subtotal.shouldBe(visible).shouldHave(exactText("99,00 LEU"));
        takeScreenShot("Subtotal is correct.");
        mainPage.isEmptyCartButtonVisible();
        mainPage.clickOnEmptyCartButton();
        mainPage.clickOnHomepageLogo();
    }


}



